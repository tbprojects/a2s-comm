# A2sComm

Moduł Angular zawierający A2sComponent, przeznaczony do realizacji szkolenia [Angular in Space](http://angular-in-space.pl).

## Instalacja

        npm install a2s-comm --save

## Użycie

Zmiana w `app.component.ts`

        import { A2sCommComponent } from 'a2s-comm';
        // ...
        @Component({
            // ...
            imports: [A2sCommComponent]
            // ...
        })
        export class AppComponent {}

Zmiana w `app.component.html`

        // ...
        <a2s-comm></a2s-comm>
        // ...
