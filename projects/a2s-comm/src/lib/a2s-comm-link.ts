export interface A2sCommLink {
    name: string;
    url: string;
    css: string;
}
