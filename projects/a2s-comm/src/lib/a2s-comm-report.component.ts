import { Component, computed, inject, OnDestroy, OnInit, output, signal } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { default as html2canvas } from 'html2canvas';
import { A2sCommService } from './a2s-comm.service';
import { A2sCommReport } from './a2s-comm-report';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'a2s-comm-report',
  templateUrl: './a2s-comm-report.component.html',
  styleUrl: './a2s-comm-report.component.css',
  imports: [ReactiveFormsModule],
})
export class A2sCommReportComponent implements OnInit, OnDestroy{
  private service = inject(A2sCommService);
  closed = output<void>();

  form = new FormGroup({
    reportDate: new FormControl('', {validators: [Validators.required]}),
    firstName: new FormControl('', {validators: [Validators.required]}),
    lastName: new FormControl('', {validators: [Validators.required]}),
    email: new FormControl('', {validators: [Validators.required, Validators.email]}),
    image: new FormControl('', {validators: [Validators.required]})
  });

  ready = signal(false);
  sending = signal(false);
  formValue = toSignal(this.form.valueChanges, {initialValue: this.form.value});
  statement = computed(() => {
    const {firstName, lastName, reportDate, email} = this.formValue();
    return `Ja, ${firstName} ${lastName} (email: ${email}), byłem obecny na szkoleniu w dniu ${reportDate}.`;
  });

  async ngOnInit() {
    window.document.body.style.overflow = 'hidden';

    const canvas = await html2canvas(document.body);

    this.form.patchValue({
      reportDate: new Date().toLocaleDateString(),
      firstName: localStorage.getItem('presence-first-name') || '',
      lastName: localStorage.getItem('presence-last-name') || '',
      email: localStorage.getItem('presence-email') || '',
      image: canvas.toDataURL('image/jpeg', 0.5)
    });

    this.ready.set(true);
  }

  ngOnDestroy() {
    window.document.body.style.overflow = 'auto';
  }

  async sendReport() {
    if (this.form.invalid) {
      alert('Wszystkie pola powinny być uzupełnione.');
      return;
    }

    const data = {...this.formValue() as A2sCommReport, statement: this.statement()};

    localStorage.setItem('presence-first-name', data.firstName);
    localStorage.setItem('presence-last-name', data.lastName);
    localStorage.setItem('presence-email', data.email);

    this.sending.set(true);

    try {
      await this.service.sendReport(data);
      alert('Informacja o obecności wysłana!');
      this.closed.emit();
    } catch (err) {
      alert('Wystąpił błąd podczas wysyłania! Jeśli problem się powtarza, wyślij proszę informację o obecności wraz z screenshotem na info@angular-in-space.pl');
      console.error(err);
    }

    this.sending.set(false);
  }
}
