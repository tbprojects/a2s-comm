import { Injectable } from '@angular/core';
import { A2sCommReport } from './a2s-comm-report';

@Injectable()
export class A2sCommService {
  async sendReport(report: A2sCommReport): Promise<unknown> {
    const response = await fetch('https://api.angular-in-space.pl/presence', {
      method: 'POST',
      body: JSON.stringify(report),
      headers: { 'Content-Type': 'application/json' },
    });
    return response.json();
  }
}
