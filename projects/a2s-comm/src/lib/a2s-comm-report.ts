export interface A2sCommReport {
  reportDate: string;
  firstName: string;
  lastName: string;
  statement: string;
  email: string;
  image: string;
}
