import { Component, signal } from '@angular/core';
import { A2sCommLink } from './a2s-comm-link';
import { A2sCommService } from './a2s-comm.service';
import { A2sCommReportComponent } from './a2s-comm-report.component';

@Component({
  selector: 'a2s-comm',
  templateUrl: './a2s-comm.component.html',
  styleUrls: ['./a2s-comm.component.css'],
  imports: [A2sCommReportComponent],
  providers: [A2sCommService],
})
export class A2sCommComponent {
  links: A2sCommLink[] = [
    {name: 'Zadania Full', url: 'http://hq.angular-in-space.pl/chapters/exercises', css: 'blue'},
    {name: 'Zadania Master', url: 'http://hq.angular-in-space.pl/chapters/exercises-advanced', css: 'blue'},
    {name: 'Wiedza', url: 'http://hq.angular-in-space.pl/chapters/learn', css: 'blue'},
    {name: 'Angular Docs', url: 'https://angular.dev/overview', css: 'red'},
    {name: 'Angular API', url: 'https://angular.dev/api', css: 'red'},
    {name: 'Angular CLI', url: 'https://angular.dev/cli', css: 'red'}
  ];

  menuOpened = signal(false);
  reportOpened = signal(false);

  toggleMenu(newValue?: boolean) {
    this.menuOpened.update(value => newValue ?? !value);
  }

  toggleReport(newValue?: boolean) {
    this.reportOpened.update(value => newValue ?? !value);
  }

  open(url: string) {
    this.toggleMenu(false);
    window.open(url, '_blank');
  }
}
